# -*- coding: utf-8 -*-

# Scrapy settings for gpu_scrapy project
#
# For simplicity, this file contains only settings considered important or
# commonly used. You can find more settings consulting the documentation:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#     http://scrapy.readthedocs.org/en/latest/topics/downloader-middleware.html
#     http://scrapy.readthedocs.org/en/latest/topics/spider-middleware.html
import sys
import os

import django

BOT_NAME = 'gpu_scrapy'

SPIDER_MODULES = ['gpu_scrapy.spiders']
NEWSPIDER_MODULE = 'gpu_scrapy.spiders'

FEED_URI = 'logs/%(name)s/%(time)s.json'
FEED_FORMAT = 'json'

os.environ['DJANGO_SETTINGS_MODULE'] = 'hotline.settings'
path = os.path.join(os.path.dirname(__file__), '../ds_data')
sys.path.append(os.path.abspath(path))
django.setup()

ITEM_PIPELINES = {
    'gpu_scrapy.pipelines.GpuScrapyPipeline': 100,
}
