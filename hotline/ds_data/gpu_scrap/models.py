from django.db import models


class DataGPU(models.Model):
    """Class to save the models in the database"""
    name = models.CharField(max_length=256)
    link = models.CharField(max_length=256)
    description = models.TextField(null=True, blank=True)

    minimal = models.TextField(null=True, blank=True)
    maximum = models.TextField(null=True, blank=True)

    manufacturer = models.TextField(null=True, blank=True)
    type = models.TextField(null=True, blank=True)
    gpu = models.TextField(null=True, blank=True)
    memory = models.TextField(null=True, blank=True)
    memory_type = models.TextField(null=True, blank=True)
    interface = models.TextField(null=True, blank=True)
    cooling_system = models.TextField(null=True, blank=True)
    work_frequency_gpu = models.TextField(null=True, blank=True)
    work_frequency_memory = models.TextField(null=True, blank=True)
    memory_bus = models.TextField(null=True, blank=True)
    output_connectors = models.TextField(null=True, blank=True)
    manufacturers_website = models.TextField(null=True, blank=True)

    name_shop = models.TextField(null=True, blank=True)
    link_to_shop = models.TextField(null=True, blank=True)
    price_in_shop = models.TextField(null=True, blank=True)
