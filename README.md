##  Installing

1. Create a folder on the local drive and clone the repository ```https://igor_peresunko@bitbucket.org/igor_peresunko/django-scrapy.git```

2. Navigate to the folder you created that contains a copy of the repository

3. To create a virtual environment ```virtualenv .env```

4. Run virtual environment ```. .env/bin/activate```

5. Run the installation script ```sh install.sh```
