import scrapy
from scrapy.selector import Selector
from scrapy.spider import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor

from gpu_scrapy.items import HotlineItem


class HotlineSpider(CrawlSpider):
    """Spider for parsing video cards on the website hotline.ua"""
    name = 'hotline'
    allowed_domains = ["hotline.ua"]
    start_urls = ['http://hotline.ua/computer/videokarty/']

    rules = [Rule(LinkExtractor(
        allow=['/computer/videokarty/\?&p=\d*']),
        callback='parse_one',
        follow=True)
    ]

    def parse_one(self, response):
        for href in response.xpath('//ul[@class="catalog  clearfix"]/li/div[3]/div[@class="title-box"]/div[1]/a/@href'):
            url = response.urljoin(href.extract())
            yield scrapy.Request(url, callback=self.parse_product)

    def parse_product(self, response):
        sel = Selector(response)
        sites = sel.xpath('//div[@class="content  full  cf"]')
        product = []

        for site in sites:
            item = HotlineItem()
            price = HotlineItem()
            features = HotlineItem()
            shops = HotlineItem()

            item['name'] = site.xpath('div[2]/h1/text()').re('[^\s]+')
            item['description'] = site.xpath('//div[@class="opis description"]/p[@class="short-desc"]/text()').extract()
            item['link'] = response.url

            minmax = site.xpath('//a[@data-id="prices"]/strong/text()').re('\d{1,2}\s\d{3}')
            price['minimal'] = minmax[0]
            price['maximum'] = minmax[1]
            item['price'] = price

            features['manufacturer'] = site.xpath('//div[@id="short-props-list"]/table/tr[1]/td/a/text()').extract()
            features['type'] = site.xpath('//div[@id="short-props-list"]/table/tr[2]/td/text()').extract()
            features['gpu'] = site.xpath('//div[@id="short-props-list"]/table/tr[3]/td/text()').extract()
            features['memory'] = site.xpath('//div[@id="short-props-list"]/table/tr[4]/td/text()').extract()
            features['memory_type'] = site.xpath('//div[@id="short-props-list"]/table/tr[5]/td/text()').extract()
            features['interface'] = site.xpath('//div[@id="short-props-list"]/table/tr[6]/td/text()').extract()
            features['cooling_system'] = site.xpath('//div[@id="short-props-list"]/table/tr[7]/td/text()').extract()
            features['work_frequency_gpu'] = site.xpath('//div[@id="short-props-list"]/table/tr[8]/td/text()').re('(?:\d*\.)?\d+')
            features['work_frequency_memory'] = site.xpath('//div[@id="short-props-list"]/table/tr[9]/td/text()').re('(?:\d*\.)?\d+')
            features['memory_bus'] = site.xpath('//div[@id="short-props-list"]/table/tr[10]/td/text()').re('(?:\d*\.)?\d+')
            features['output_connectors'] = site.xpath('//div[@id="short-props-list"]/table/tr[11]/td/text()').extract()
            features['manufacturers_website'] = site.xpath('//div[@id="short-props-list"]/table/tr[12]/td/a/@href').extract()
            item['features'] = features

            shops['name_shop'] = site.xpath('//div[@class="th-prices"]/table/tr/th[1]/p/a/text()').extract()
            shops['link_to_shop'] = site.xpath('//div[@class="th-prices"]/table/tr/th[1]/p/a/@href').extract()
            shops['price_in_shop'] = site.xpath('//div[@class="th-prices"]/table/tr/td[2]/a/b/text()').re('\d{1,}\s\d{3}')
            item['shops'] = shops

            product.append(item)

        return product


class TestHotline(scrapy.Spider):
    """Class to test ideas"""
    name = 'testhot'
    allowed_domains = ["hotline.ua"]
    start_urls = ['http://hotline.ua/computer/videokarty/']

    def parse(self, response):
        for href in response.xpath('//ul[@class="catalog  clearfix"]/li/div[3]/div[@class="title-box"]/div[1]/a/@href'):
            url = response.urljoin(href.extract())
            yield scrapy.Request(url, callback=self.parse_product)

    def parse_product(self, response):
        sel = Selector(response)
        sites = sel.xpath('//div[@class="content  full  cf"]')
        product = []

        for site in sites:
            item = HotlineItem()
            # price = HotlineItem()
            # shops = HotlineItem()
            # features = HotlineItem()

            item['name'] = site.xpath('div[2]/h1/text()').re('[^\s]+')
            item['description'] = site.xpath('//p[@class="full-desc"]/text()').extract()
            item['link'] = response.url

            # minmax = site.xpath('//a[@data-id="prices"]/strong/text()').re('\d{1,2}\s\d{3}')
            # price['minimal'] = minmax[0]
            # price['maximum'] = minmax[1]
            # item['price'] = price
            #
            # features['manufacturer'] = site.xpath('//div[@id="short-props-list"]/table/tr[1]/td/a/text()').extract()
            # features['type'] = site.xpath('//div[@id="short-props-list"]/table/tr[2]/td/text()').extract()
            # features['gpu'] = site.xpath('//div[@id="short-props-list"]/table/tr[3]/td/text()').extract()
            # features['memory'] = site.xpath('//div[@id="short-props-list"]/table/tr[4]/td/text()').extract()
            # features['memory_type'] = site.xpath('//div[@id="short-props-list"]/table/tr[5]/td/text()').extract()
            # features['interface'] = site.xpath('//div[@id="short-props-list"]/table/tr[6]/td/text()').extract()
            # features['cooling_system'] = site.xpath('//div[@id="short-props-list"]/table/tr[7]/td/text()').extract()
            # features['work_frequency_gpu'] = site.xpath('//div[@id="short-props-list"]/table/tr[8]/td/text()').re('(?:\d*\.)?\d+')
            # features['work_frequency_memory'] = site.xpath('//div[@id="short-props-list"]/table/tr[9]/td/text()').re('(?:\d*\.)?\d+')
            # features['memory_bus'] = site.xpath('//div[@id="short-props-list"]/table/tr[10]/td/text()').re('(?:\d*\.)?\d+')
            # features['output_connectors'] = site.xpath('//div[@id="short-props-list"]/table/tr[11]/td/text()').extract()
            # features['manufacturers_website'] = site.xpath('//div[@id="short-props-list"]/table/tr[12]/td/a/@href').extract()
            # item['features'] = features
            #
            # shops['name_shop'] = site.xpath('//div[@class="th-prices"]/table/tr/th[1]/p/a/text()').extract()
            # shops['link_to_shop'] = site.xpath('//div[@class="th-prices"]/table/tr/th[1]/p/a/@href').extract()
            # shops['price_in_shop'] = site.xpath('//div[@class="th-prices"]/table/tr/td[2]/a/b/text()').re('\d{1,}\s\d{3}')
            # item['shops'] = shops

            product.append(item)

        return product