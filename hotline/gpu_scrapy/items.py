# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy
from scrapy_djangoitem import DjangoItem

from gpu_scrap.models import DataGPU


class HotlineItem(DjangoItem):
    """Items for binding to a Django model"""
    django_model = DataGPU
    name = scrapy.Field()
    link = scrapy.Field()
    description = scrapy.Field()

    price = scrapy.Field()
    minimal = scrapy.Field()
    maximum = scrapy.Field()

    features = scrapy.Field()
    manufacturer = scrapy.Field()
    type = scrapy.Field()
    gpu = scrapy.Field()
    memory = scrapy.Field()
    memory_type = scrapy.Field()
    interface = scrapy.Field()
    cooling_system = scrapy.Field()
    work_frequency_gpu = scrapy.Field()
    work_frequency_memory = scrapy.Field()
    memory_bus = scrapy.Field()
    output_connectors = scrapy.Field()
    manufacturers_website = scrapy.Field()

    shops = scrapy.Field()
    name_shop = scrapy.Field()
    link_to_shop = scrapy.Field()
    price_in_shop = scrapy.Field()
