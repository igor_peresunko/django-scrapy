# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
from gpu_scrap.models import DataGPU


class GpuScrapyPipeline(object):
    def process_item(self, item, spider):
        print('Ok')
        item.save()
        return item
